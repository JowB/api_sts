<?php

namespace App\Controller;

use App\Entity\Sneaker;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Encoder\XmlEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;

/**
 * Class MainController
 * @property Serializer serializer
 * @package App\Controller
 * @Route("/main")
 */
class MainController extends Controller
{
    public function __construct()
    {
        $encoder = new JsonEncoder();
        $normalizer = new ObjectNormalizer();

        $normalizer->setCircularReferenceHandler(function ($object) {
            return $object->getId();
        });
        $normalizer->setIgnoredAttributes(['brand', 'category', 'cartSneakers', 'colors']);
        $this->serializer = new Serializer(array($normalizer), array($encoder));

    }

    /**
     * @Route("/bycategory/{id}", name="find_by_category")
     * @Method({"GET"})
     * @param $id
     * @return Response
     */
    public function findByCategory($id)
    {
        $data = $this->getDoctrine()->getRepository(Sneaker::class)->findAllByCategory($id);
        $i = 0;
        foreach ($data as $sneaker) {
            $sneaker['images'] = explode(",", $sneaker['images']);
            $data[$i] = $sneaker;
            $i++;
        }
        $json = $this->serializer->serialize($data, 'json');
        return new Response($json);
    }

    /**
     * @Route("/sneakerbyid/{id}", name="sneaker_by_id")
     * @Method({"GET"})
     * @param $id
     * @return Response
     */
    public function sneakerById($id)
    {
        $data = $this->getDoctrine()->getRepository(Sneaker::class)->findAllByRelation($id);
        $size = explode(",", $data['sizes']);
        $data['sizes'] = $size;
        $image = explode(",", $data['images']);
        $data['images'] = $image;
        $json = $this->serializer->serialize($data, 'json');
        return new Response($json);
    }
}
