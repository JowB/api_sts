<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\CartSneakersRepository")
 */
class CartSneakers
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Sneaker", inversedBy="cartSneakers")
     */
    private $sneakers;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Cart", inversedBy="cartSneakers")
     */
    private $cart;

    public function getId()
    {
        return $this->id;
    }

    public function getSneakers(): ?Sneaker
    {
        return $this->sneakers;
    }

    public function setSneakers(?Sneaker $sneakers): self
    {
        $this->sneakers = $sneakers;

        return $this;
    }

    public function getCart(): ?Cart
    {
        return $this->cart;
    }

    public function setCart(?Cart $cart): self
    {
        $this->cart = $cart;

        return $this;
    }
}
