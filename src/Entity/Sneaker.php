<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\MaxDepth;

/**
 * @ORM\Entity(repositoryClass="App\Repository\SneakerRepository")
 */
class Sneaker
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="integer")
     */
    private $price;

    /**
     * @MaxDepth(2)
     * @ORM\OneToMany(targetEntity="App\Entity\Color", mappedBy="sneaker", orphanRemoval=true)
     */
    private $colors;

    /**
     * @MaxDepth(2)
     * @ORM\OneToMany(targetEntity="App\Entity\Size", mappedBy="sneaker", orphanRemoval=true)
     */
    private $sizes;


    /**
     * @MaxDepth(1)
     * @ORM\ManyToOne(targetEntity="App\Entity\Brand", inversedBy="sneakers")
     * @ORM\JoinColumn(nullable=false)
     */
    private $brand;

    /**
     * @MaxDepth(1)
     * @ORM\ManyToOne(targetEntity="App\Entity\Category", inversedBy="sneakers")
     * @ORM\JoinColumn(nullable=false)
     */
    private $category;

    /**
     * @MaxDepth(1)
     * @ORM\OneToMany(targetEntity="App\Entity\CartSneakers", mappedBy="sneakers")
     */
    private $cartSneakers;

    /**
     * @MaxDepth(2)
     * @ORM\OneToMany(targetEntity="App\Entity\Image", mappedBy="sneaker", orphanRemoval=true)
     */
    private $images;


    public function __construct()
    {
        $this->colors = new ArrayCollection();
        $this->sizes = new ArrayCollection();
        $this->cartSneakers = new ArrayCollection();
        $this->images = new ArrayCollection();
    }

    public function getId()
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getPrice(): ?int
    {
        return $this->price;
    }

    public function setPrice(int $price): self
    {
        $this->price = $price;

        return $this;
    }

    /**
     * @return Collection|Color[]
     */
    public function getColors(): Collection
    {
        return $this->colors;
    }

    public function addColor(Color $color): self
    {
        if (!$this->colors->contains($color)) {
            $this->colors[] = $color;
            $color->setSneaker($this);
        }

        return $this;
    }

    public function removeColor(Color $color): self
    {
        if ($this->colors->contains($color)) {
            $this->colors->removeElement($color);
            // set the owning side to null (unless already changed)
            if ($color->getSneaker() === $this) {
                $color->setSneaker(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Size[]
     */
    public function getSizes(): Collection
    {
        return $this->sizes;
    }

    public function addSize(Size $size): self
    {
        if (!$this->sizes->contains($size)) {
            $this->sizes[] = $size;
            $size->setSneaker($this);
        }

        return $this;
    }

    public function removeSize(Size $size): self
    {
        if ($this->sizes->contains($size)) {
            $this->sizes->removeElement($size);
            // set the owning side to null (unless already changed)
            if ($size->getSneaker() === $this) {
                $size->setSneaker(null);
            }
        }

        return $this;
    }


    public function getBrand(): ?Brand
    {
        return $this->brand;
    }

    public function setBrand(?Brand $brand): self
    {
        $this->brand = $brand;

        return $this;
    }

    public function getCategory(): ?Category
    {
        return $this->category;
    }

    public function setCategory(?Category $category): self
    {
        $this->category = $category;

        return $this;
    }

    /**
     * @return Collection|CartSneakers[]
     */
    public function getCartSneakers(): Collection
    {
        return $this->cartSneakers;
    }

    public function addCartSneaker(CartSneakers $cartSneaker): self
    {
        if (!$this->cartSneakers->contains($cartSneaker)) {
            $this->cartSneakers[] = $cartSneaker;
            $cartSneaker->setSneakers($this);
        }

        return $this;
    }

    public function removeCartSneaker(CartSneakers $cartSneaker): self
    {
        if ($this->cartSneakers->contains($cartSneaker)) {
            $this->cartSneakers->removeElement($cartSneaker);
            // set the owning side to null (unless already changed)
            if ($cartSneaker->getSneakers() === $this) {
                $cartSneaker->setSneakers(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Image[]
     */
    public function getImages(): Collection
    {
        return $this->images;
    }

    public function addImage(Image $image): self
    {
        if (!$this->images->contains($image)) {
            $this->images[] = $image;
            $image->setSneaker($this);
        }

        return $this;
    }

    public function removeImage(Image $image): self
    {
        if ($this->images->contains($image)) {
            $this->images->removeElement($image);
            // set the owning side to null (unless already changed)
            if ($image->getSneaker() === $this) {
                $image->setSneaker(null);
            }
        }

        return $this;
    }
}
