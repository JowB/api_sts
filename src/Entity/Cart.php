<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\CartRepository")
 */
class Cart
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\User", inversedBy="cart", cascade={"persist", "remove"})
     */
    private $user;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\CartSneakers", mappedBy="cart")
     */
    private $cartSneakers;

    public function __construct()
    {
        $this->cartSneakers = new ArrayCollection();
    }

    public function getId()
    {
        return $this->id;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }

    /**
     * @return Collection|CartSneakers[]
     */
    public function getCartSneakers(): Collection
    {
        return $this->cartSneakers;
    }

    public function addCartSneaker(CartSneakers $cartSneaker): self
    {
        if (!$this->cartSneakers->contains($cartSneaker)) {
            $this->cartSneakers[] = $cartSneaker;
            $cartSneaker->setCart($this);
        }

        return $this;
    }

    public function removeCartSneaker(CartSneakers $cartSneaker): self
    {
        if ($this->cartSneakers->contains($cartSneaker)) {
            $this->cartSneakers->removeElement($cartSneaker);
            // set the owning side to null (unless already changed)
            if ($cartSneaker->getCart() === $this) {
                $cartSneaker->setCart(null);
            }
        }

        return $this;
    }
}
