<?php

namespace App\Repository;

use App\Entity\Sneaker;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Sneaker|null find($id, $lockMode = null, $lockVersion = null)
 * @method Sneaker|null findOneBy(array $criteria, array $orderBy = null)
 * @method Sneaker[]    findAll()
 * @method Sneaker[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class SneakerRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Sneaker::class);
    }

        public function findAllByCategory($id)
    {
        $conn = $this->getEntityManager()->getConnection();

        $sql = '
            SELECT s.id, s.name, s.price, b.name as marque, (SELECT GROUP_CONCAT(url) FROM image WHERE sneaker_id = s.id) as images FROM `sneaker` as s
            INNER JOIN category as c
            ON c.id = s.category_id
            INNER JOIN brand as b 
            ON b.id = s.brand_id
            WHERE c.id = :id
        ';
        $stmt = $conn->prepare($sql);
        $stmt->execute(['id' => $id]);

        // returns an array of arrays (i.e. a raw data set)
        return $stmt->fetchAll();
    }

    public function findAllByRelation($id)
    {
        $conn = $this->getEntityManager()->getConnection();

        // SELECT sneak.id, sneak.name, sneak.price, c.name as category, b.name as marque, size.number FROM `sneaker` as sneak INNER JOIN category as c ON c.id = sneak.category_id INNER JOIN brand as b ON b.id = sneak.brand_id RIGHT JOIN `size` ON size.sneaker_id = sneak.id WHERE sneak.id = :id
        $sql = '
            SELECT sneak.id, sneak.name, sneak.price, c.name as category, b.name as marque, (SELECT GROUP_CONCAT(number) FROM size WHERE sneaker_id = :id) as sizes, (SELECT GROUP_CONCAT(url) FROM image WHERE sneaker_id = sneak.id) as images FROM `sneaker` as sneak
            INNER JOIN category as c
            ON c.id = sneak.category_id
            INNER JOIN brand as b 
            ON b.id = sneak.brand_id
            WHERE sneak.id = :id
        ';
        $stmt = $conn->prepare($sql);
        $stmt->execute(['id' => $id]);

        // returns an array of arrays (i.e. a raw data set)
        return $stmt->fetch();
    }
}
