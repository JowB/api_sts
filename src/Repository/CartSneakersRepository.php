<?php

namespace App\Repository;

use App\Entity\CartSneakers;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method CartSneakers|null find($id, $lockMode = null, $lockVersion = null)
 * @method CartSneakers|null findOneBy(array $criteria, array $orderBy = null)
 * @method CartSneakers[]    findAll()
 * @method CartSneakers[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CartSneakersRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, CartSneakers::class);
    }

//    /**
//     * @return CartSneakers[] Returns an array of CartSneakers objects
//     */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?CartSneakers
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
